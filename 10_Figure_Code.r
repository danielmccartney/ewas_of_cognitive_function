################
### Figure 1 ###
################

plotdata = readRDS("/Cluster_Filespace/Marioni_Group/Daniel/Cog_Combined_BayesR/plotdata.rds")

levels(plotdata$Data)[levels(plotdata$Data)=="GWAS"] <- "SNP"
levels(plotdata$Cognitive_Phenotype)[levels(plotdata$Cognitive_Phenotype)=="Verbal Total"] <- "Verbal Fluency"
require(RColorBrewer)
colours = brewer.pal(12, "Paired")
require(ggplot2)

fig1b = ggplot(plotdata, aes(x=Cognitive_Phenotype, y=Mean_Variance_Explained, fill=Data)) + 
		geom_bar(stat="identity", position="dodge") +
		geom_errorbar(aes(ymin=Variance_LCI, ymax=Variance_HCI), width=.2, position=position_dodge(.9)) + 
		scale_fill_manual(values=colours[c(3,7,9)])+
		ylab("Mean Variance Explained - 95% Credible Interval") +
		xlab("Cognitive Trait") +
		ylim(0,1) + 
		ggtitle("DNAm and SNP Variance Components Analysis") + 
		theme_light() +
		theme(axis.title.x = element_text(size = 16),
		       axis.title.y = element_text(size = 16), 
               axis.text=element_text(size=14),
			   plot.title=element_text(size = 16),
               legend.text=element_text(size=16),
			   legend.position=c(0.9,0.85),
			   legend.background = element_rect(size=0.5, linetype="solid", colour="black"),
			   legend.title=element_blank()) 


# Plot of DNAm heritabilities
# Heritability (small/medium/large-effects probes)
require(reshape2)
require(ggplot2)
bayes =  read.csv("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/Cog_Summary/Var_Explained_Combined_output_10k.csv")

bayes$Small_Effects = bayes$Mean_Variance_Explained * bayes$Proportion_Small_Effects
bayes$Medium_Effects = bayes$Mean_Variance_Explained * bayes$Proportion_Medium_Effects
bayes$Large_Effects = bayes$Mean_Variance_Explained * bayes$Proportion_Large_Effects
bayes = melt(bayes)
bayes2 = droplevels(bayes[which(bayes$variable %in% c("Small_Effects", "Medium_Effects", "Large_Effects")), ])
bayes2$variable = factor(bayes2$variable, levels=c("Large_Effects", "Medium_Effects", "Small_Effects"))

bayes2$Biomarker2 = as.character(bayes2$Biomarker)
bayes2$Biomarker2 = gsub("Digit", "Digit Symbol", bayes2$Biomarker2)
bayes2$Biomarker2 = gsub("\\bG\\b", "g", bayes2$Biomarker2)
bayes2$Biomarker2 = gsub("GF", "gf", bayes2$Biomarker2)
bayes2$Biomarker2 = gsub("Verbal", "Verbal Fluency", bayes2$Biomarker2)
bayes2$Biomarker2 = gsub("LM", "Logical Memory", bayes2$Biomarker2)

bayes2$Biomarker2 = gsub("_10k|_processed", "", bayes2$Biomarker2)

lows = bayes[which(bayes$variable=="Variance_LCI"),]
highs = bayes[which(bayes$variable=="Variance_HCI"),]

bayes2$LCI = lows[match(bayes2$Biomarker, lows$Biomarker), "value"]
bayes2$UCI = highs[match(bayes2$Biomarker, highs$Biomarker), "value"]

require(RColorBrewer)
colours = brewer.pal(12, "Paired")
bayes2$fill = colours[as.numeric(bayes2$variable)]

bayes2$Biomarker3 <- factor(bayes2$Biomarker2, levels=c("Logical Memory", "Digit Symbol", "Verbal Fluency", "Vocabulary","gf","g"))
levels(bayes2$variable)[levels(bayes2$variable)=="Small_Effects"] <- "Small Effects (mixture variance 0.01%)"
levels(bayes2$variable)[levels(bayes2$variable)=="Medium_Effects"] <- "Medium Effects (mixture variance 0.1%)"
levels(bayes2$variable)[levels(bayes2$variable)=="Large_Effects"] <- "Large Effects (mixture variance 1%)"


fig1a =  ggplot(data=bayes2) + 
		geom_bar(aes(y=value, x=Biomarker3,fill=variable), stat="identity") + 
		scale_fill_manual(values=colours[c(3,7,9)]) + 
		ylim(0,1) + 
		ylab("Mean Variance Explained - 95% Credible Interval") + 
		xlab("Cognitive Trait") + 
		geom_errorbar(aes(x=Biomarker3, ymin=LCI, ymax=UCI), position="identity", width=0.6)+
		theme_light() +
		ggtitle("DNAm Variance Components Analysis") + 
		theme(axis.title.x = element_text(size = 16),
		       axis.title.y = element_text(size = 16), 
               axis.text=element_text(size=14),
			   plot.title=element_text(size = 16),
               legend.text=element_text(size=16),
			   legend.position=c(0.75,0.85),
			   legend.background = element_rect(size=0.5, linetype="solid", colour="black"),
			   legend.title=element_blank())


require(gridExtra)

tiff("Figure_1_April2021.tiff", width=800, height=800)
grid.arrange(fig1a, fig1b, nrow=2)
dev.off() 

#############################
### Supplementary Table 3 ###
#############################

table_s3 <- as.data.frame(cbind(bayes2$Biomarker[1:6], bayes2$value[1:6], bayes2$value[7:12],bayes2$value[13:18]) )
names(table_s3) <- c("Cognitive Test","Small","Medium","Large")
table_s3$Total <- as.numeric(table_s3$Small) + as.numeric(table_s3$Medium) + as.numeric(table_s3$Large)
table_s3[c(1,4,5,6,3,2),]


################
### Figure 2 ###
################

require(RColorBrewer)
colours = brewer.pal(12, "Paired")
require(ggplot2)

r2_36 <- c(3.4, 7.3, 10.7)
ph <- c("DNAm","SNP","DNAm + SNP")
fig_dat36 <- data.frame(cbind(ph, r2_36))
fig_dat36$r2_36 <- as.numeric(fig_dat36$r2_36)
fig_dat36$ph1 <- factor(fig_dat36$ph, levels=c("DNAm","SNP","DNAm + SNP"))


bar36 <- ggplot(fig_dat36, aes(x=ph1, y=r2_36, fill=ph)) + 
	geom_bar(stat="identity") +
	scale_fill_manual(values=colours[c(3,7,9)])+
	ylab("Incremental R2 (%)") +
	xlab("Omics data type") +
	ylim(0,15) + 
	ggtitle("DNAm and SNP polygenic prediction of g in LBC1936") + 
	theme_light() +
	theme(axis.title.x = element_text(size = 16),
	axis.title.y = element_text(size = 16), 
        axis.text=element_text(size=14),
	plot.title=element_text(size = 16),
	legend.position = "none") 

r2_21 <- c(4.5, 6.9, 10.5)
ph <- c("DNAm","SNP","DNAm + SNP")
fig_dat21 <- data.frame(cbind(ph, r2_21))
fig_dat21$r2_21 <- as.numeric(fig_dat21$r2_21)
fig_dat21$ph1 <- factor(fig_dat21$ph, levels=c("DNAm","SNP","DNAm + SNP"))

bar21 <- ggplot(fig_dat21, aes(x=ph1, y=r2_21, fill=ph)) + 
	geom_bar(stat="identity") +
	scale_fill_manual(values=colours[c(3,7,9)])+
	ylab("Incremental R2 (%)") +
	xlab("Omics data type") +
	ylim(0,15) + 
	ggtitle("DNAm and SNP polygenic prediction of g in LBC1921") + 
	theme_light() +
	theme(axis.title.x = element_text(size = 16),
	axis.title.y = element_text(size = 16), 
        axis.text=element_text(size=14),
	plot.title=element_text(size = 16),
	legend.position = "none") 

require(gridExtra)
#tiff("Figure_2_April2021.tiff", width=800, height=800)
grid.arrange(bar36, bar21, nrow=1)

##############################
### Supplementary Figure 1 ###
##############################

d = read.csv("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/Cognitive_BayesR_10k_phenotypes.csv")
mean(d$EpiScore)
sd(d$EpiScore)
length(!is.na(d$EpiScore))

smk <- read.csv("/Cluster_Filespace/Marioni_Group/GS/GS_dataset/ever_smoke.csv")
smk1 <- merge(d, smk, by="Sample_Name")
smk1$smk_cat <- smk1$ever_smoke
smk1$smk_cat[smk1$smk_cat==5] <- NA
smk1$smk_cat[smk1$smk_cat==1] <- "Current \nN=1,530"
smk1$smk_cat[smk1$smk_cat==2] <- "Former (<12 months)\nN=218"
smk1$smk_cat[smk1$smk_cat==3] <- "Former (>12 months)\nN=2,513"
smk1$smk_cat[smk1$smk_cat==4] <- "Never\nN=4,650"
smk1$smk_cat[is.na(smk1$smk_cat)] <- "Unknown\nN=251"
table(smk1$smk_cat, smk1$ever_smoke)
  
library(ggplot2)

ggplot(smk1, aes(x=smk_cat, y=EpiScore)) + 
	geom_violin(trim=FALSE, fill="gray")+
	labs(title="EpiSmoker vs Self-report Smoking",x="Self-reported Smoking", y = "EpiSmoker Score")+
	geom_boxplot(width=0.1)+
	theme_classic() +
	theme(axis.title.x = element_text(size = 16),
		axis.title.y = element_text(size = 16), 
		axis.text=element_text(size=14),
		plot.title=element_text(size = 16))
  

##############################
### Supplementary Figure 2 ###
##############################

group_pips = readRDS("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/group_pips.rds")
pips = readRDS("pips.rds")
sigs = list()
for(i in names(group_pips)){
  tmp = names(group_pips[[i]])[which(group_pips[[i]] > 0.8)] # Lead CpGs with group pips>0.8
  sigs[[i]] = pips[[i]][which(pips[[i]]$CpG %in% tmp),]
}
sigs = do.call("rbind", sigs)
plotdat = sigs
plotdat$Trait = gsub("_processed*.*", "", rownames(plotdat))
plotdat$Beta = plotdat$Median_Beta
plotdat$LCI = plotdat$Beta_2.5
plotdat$HCI = plotdat$Beta_97.5
require(RColorBrewer)
colours = brewer.pal(12, "Paired")
colvalues=colours[c(1,3,5,7,9)]
require(ggplot2)

plotdat$lci <- plotdat$Beta_5
plotdat$uci <- plotdat$Beta_95

plotdat$CpG1 <- factor(plotdat$CpG, levels=c("cg05325763", "cg13108341", "cg09984392"))
plotdat$Trait1 <- factor(plotdat$Trait, levels=c("Digit", "Verbal", "Vocabulary","gf","g"))
levels(plotdat$Trait1)[levels(plotdat$Trait1)=="Digit"] <- "Digit Symbol"
levels(plotdat$Trait1)[levels(plotdat$Trait1)=="Verbal"] <- "Verbal Fluency"

ggplot(plotdat, aes(x=CpG1, y=Beta, ymin=lci, ymax=uci, col=Trait1, fill=Trait1)) + 
         geom_point(size=4, position=position_dodge(width = 0.9)) + 
         geom_linerange(size=2, position=position_dodge(width = 0.9), show.legend=FALSE) +
         geom_linerange(aes(ymin=LCI, ymax=HCI), size=1, position=position_dodge(width = 0.9), show.legend=FALSE) +
         scale_fill_manual(values=colvalues) +
         scale_color_manual(values=colvalues) +
         coord_flip() + 
         ylim(-0.13,0.11) + 
         ggtitle("Median Betas and 95% Credible Intervals") + 
         theme_light() + 
		 ylab("Effect Size (Beta)") +
		 xlab("CpG") +
		 theme(axis.title.x = element_text(size = 16),
		       axis.title.y = element_text(size = 16), 
               plot.title=element_text(size = 16),
			   axis.text=element_text(size=16),
               legend.text=element_text(size=16),
			   legend.position=c(0.8,0.2),
			   legend.background = element_rect(size=0.5, linetype="solid", colour="black"),
			   legend.title=element_blank())


##############################
### Supplementary Figure 3 ###
##############################

p36 <- ggplot(w1_36, aes(x=g_pred, y=g)) + 
  geom_point()+
  geom_smooth(method=lm) +
  theme_classic() +
  labs(title="LBC1936",x="Epigenetic g Score", y = "Measured g Score") +
  theme(axis.title.x = element_text(size = 16),
  axis.title.y = element_text(size = 16), 
  axis.text=element_text(size=14),
  plot.title=element_text(size = 16))

p21 <- ggplot(w1_21, aes(x=g_pred, y=g)) + 
  geom_point()+
  geom_smooth(method=lm) +
  theme_classic() +
  labs(title="LBC1921",x="Epigenetic g Score", y = "Measured g Score") +
  theme(axis.title.x = element_text(size = 16),
  axis.title.y = element_text(size = 16), 
  axis.text=element_text(size=14),
  plot.title=element_text(size = 16))
 
require(gridExtra)
#tiff("Figure_2_April2021.tiff", width=800, height=800)
grid.arrange(p36, p21, nrow=2)
#dev.off() 





 
  
