########################################################
### GS Age demographics for results in the main text ###
########################################################

d <- read.csv("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/Cognitive_BayesR_10k_phenotypes.csv")
summary(d$age)
sd(d$age)

###########################################################################################################
### Script to generate DNAm predictor of g in LBC1936 at wave 1 (age 70) and LBC1921 at wave 1 (age 79) ###
###########################################################################################################

### read in weights ###
g_wts <- read.table("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/Cog_Summstats/g_processed_Mean_Beta_PIP.txt", header=T, stringsAsFactors=F)
meanBetas <- g_wts[,1:2]

### load LBC target file ###
d = readRDS("/Cluster_Filespace/Marioni_Group/LBC/LBC_methylation/targets_3489_bloodonly.rds")
d36_w1 <- d[d$cohort=="LBC36" & d$WAVE==1 & d$set==1,]
d21_w1 <- d[d$cohort=="LBC21" & d$WAVE==1 & d$set==1,]
 
### load in methylation data ###
dat <- readRDS("/Cluster_Filespace/Marioni_Group/LBC/LBC_methylation/LBC_betas_3489_bloodonly.rds")
meth = t(dat)
meth1 = as.data.frame(meth)
meth1$id = as.character(rownames(meth1))

### subset DNAm object to LBC36 wave 1 data ###
tmp36 <- which(rownames(meth1) %in% d36_w1$Basename)
meth36 <- meth1[tmp36,]

tmp21 <- which(rownames(meth1) %in% d21_w1$Basename)
meth21 <- meth1[tmp21,]

### subset to relevant CpG sites ###
a36 = which(names(meth36) %in% meanBetas$CpG)
meth36a <- meth36[,a36]

a21 = which(names(meth21) %in% meanBetas$CpG)
meth21a <- meth21[,a21]

### scale methylation data ###
meth36b <- scale(meth36a)
meth21b <- scale(meth21a)

### replace missing values with 0 ###
meth36b[is.na(meth36b)] <- 0
meth21b[is.na(meth21b)] <- 0

### line up weights and CpGs ###
b36 = which(meanBetas$CpG %in% colnames(meth36b)) 
mean_betas <- meanBetas[b36,]
meth36c <- meth36b[,match(colnames(meth36b), mean_betas$CpG)]
meth36d <- meth36c[,mean_betas$CpG]

b21 = which(meanBetas$CpG %in% colnames(meth21b)) 
mean_betas <- meanBetas[b21,]
meth21c <- meth21b[,match(colnames(meth21b), mean_betas$CpG)]
meth21d <- meth21c[,mean_betas$CpG]

### create predictor ###
g_pred36 <- meth36d %*% mean_betas$Mean_Beta
pred36 <- as.data.frame(g_pred36)
names(pred36) <- c("g_pred")
pred36$Basename <- rownames(pred36)
d36 <- merge(d36_w1, pred36, by="Basename")

g_pred21 <- meth21d %*% mean_betas$Mean_Beta
pred21 <- as.data.frame(g_pred21)
names(pred21) <- c("g_pred")
pred21$Basename <- rownames(pred21)
d21 <- merge(d21_w1, pred21, by="Basename")


#######################################
### LBC1936 phenotypes and analyses ###
#######################################

### load in phenotypes ###
library("foreign")
ph36 <- read.spss("/Cluster_Filespace/Marioni_Group/Riccardo/Cognitive_EWAS_GS_LBC/LBC1936_BloodBasedEWASofCognitiveAbilities_RM_06MAY2021.sav", to.data.frame=T, use.value.labels=F)

### take max grip strength, recode NA SIMD (99999) value ###
ph36$grip <- pmax(ph36$griplh_w1, ph36$griprh_w1)
ph36$depind_w1[ph36$depind_w1==99999] <- NA

### remove outliers (based on visual inspection of histograms) and impossible values ###
ph36$symsear_w1[ph36$symsear_w1<0] <- NA

### merge phenos with DNAm predictor ###
d36a = merge(d36, ph36[,c(1,4:17,20:23)], by.x="ID_raw", by.y="lbc36no")

### Merge in PRS derived from Davies et al. 2018 Nat Comms Z-scores - retain predictor with P<1 ###
prs36 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/PRS/cog_prs.all.score", header=T)
tmp = merge(d36a, prs36[,c("IID","X1.000000")], by.x="ID_raw", by.y="IID", all.x=T)
tmp1 <- tmp[,c(4,6,14,19:38)]

### remove anyone with missing cognitive data ###
a <- rowSums(tmp1[,5:11])
w1_36 <- tmp1[which(!is.na(a)), ]

### adjust lung function for age, sex, and height ###
w1_36$lung <- resid(lm(fev_w1 ~ age + sex + height_w1, na.action=na.exclude, data=w1_36))

### PCA of cognitive test data at wave 1 of LBC36 ###
library(psych)
w1_36$g <- scale(principal(w1_36[,5:11], rotate="none", nfactors=1)$scores)

### PCA output for variance explained by g ###
principal(w1_36[,5:11], rotate="none", nfactors=7)


#######################################
### Main LBC1936 Prediction Results ###
#######################################

### correlation and variance explained ###
r <- cor(w1_36$g, w1_36$g_pred, use="pairwise.complete.obs")
r

### incremental DNAm R2 ###
null <- summary(lm(g ~ age + sex, data=w1_36))$r.squared
full <- summary(lm(g ~ age + sex + g_pred, data=w1_36))$r.squared
round(100*(full - null), 3)

### incremental PRS R2 ###
full2 <- summary(lm(g ~ age + sex + X1.000000, data=w1_36))$r.squared
round(100*(full2 - null), 3)

### incremental DNAm + PRS R2 ###
full3 <- summary(lm(g ~ age + sex + g_pred + X1.000000, data=w1_36))$r.squared
round(100*(full3 - null), 3)

### get pvals ###
summary(lm(g ~ age + sex + g_pred, data=w1_36))
summary(lm(g ~ age + sex + g_pred + X1.000000, data=w1_36))

### difference in g between top and bottom decile of DNAm_g ###
w1_36$g_pred_dec <- as.numeric(cut(w1_36$g_pred, quantile(w1_36$g_pred, prob = seq(0, 1, length = 11)), include.lowest=T ))
summary(lm(g ~ age + sex + factor(g_pred_dec), data=w1_36))


### Fully adjusted incremental DNAm R2 ###
nullf <- summary(lm(g ~ age + sex + log(bmi_w1) + yrsedu_w1 + factor(smokcat_w1) + log(alcunitwk_w1+1) + hibp_w1 + hadsd_w1 + depind_w1 + log(sixmwk_w1) + grip + lung, data=w1_36))$r.squared
fullf <- summary(lm(g ~ age + sex + log(bmi_w1) + yrsedu_w1 + factor(smokcat_w1) + log(alcunitwk_w1+1) + hibp_w1 + hadsd_w1 + depind_w1 + log(sixmwk_w1) + grip + lung + g_pred, data=w1_36))$r.squared
round(100*(fullf - nullf), 3)

### Fully adjusted incremental PRS R2 ###
full2f <- summary(lm(g ~ age + sex + log(bmi_w1) + yrsedu_w1 + factor(smokcat_w1) + log(alcunitwk_w1+1) + hibp_w1 + hadsd_w1 + depind_w1 + log(sixmwk_w1) + grip + lung + X1.000000, data=w1_36))$r.squared
round(100*(full2f - nullf), 3)

### Fully adjusted incremental DNAm + PRS R2 ###
full3f <- summary(lm(g ~ age + sex + log(bmi_w1) + yrsedu_w1 + factor(smokcat_w1) + log(alcunitwk_w1+1) + hibp_w1 + hadsd_w1 + depind_w1 + log(sixmwk_w1) + grip + lung + g_pred + X1.000000, data=w1_36))$r.squared
round(100*(full3f - nullf), 3)

### Get pvals for EpiScore and PGRS ###
summary(lm(g ~ age + sex + log(bmi_w1) + yrsedu_w1 + factor(smokcat_w1) + log(alcunitwk_w1+1) + hibp_w1 + hadsd_w1 + depind_w1 + log(sixmwk_w1) + grip + lung + g_pred, data=w1_36))
summary(lm(g ~ age + sex + log(bmi_w1) + yrsedu_w1 + factor(smokcat_w1) + log(alcunitwk_w1+1) + hibp_w1 + hadsd_w1 + depind_w1 + log(sixmwk_w1) + grip + lung + X1.000000, data=w1_36))

#######################################
### LBC1921 phenotypes and analyses ###
#######################################

### load in phenotypes ###
pheno21 <- read.spss("/Cluster_Filespace/Marioni_Group/Riccardo/Cognitive_EWAS_GS_LBC/LBC1921_BloodBasedEWASofCognitiveAbilities_RM_06MAY2021.sav", to.data.frame=T, use.value.labels=F)

### recode unsure history of hibp as missing (n=4) ###
pheno21$hyphist[pheno21$hyphist==3] <- NA

### merge phenos with DNAm predictor ###
d21a = merge(d21, pheno21[,c(1,4:10,12,13,15:20)], by.x="ID_raw", by.y="studyno")

### Merge in PRS derived from Davies et al. 2018 Nat Comms Z-scores - retain predictor with P<1 ###
prs21 = read.table("/Cluster_Filespace/Marioni_Group/Daniel/Cog_EWAS_BayesR/PRS/cog_prs_lbc21.all.score", header=T)
lbc_bld = read.table("/Cluster_Filespace/Riccardo/LBC1921_FullSampleSex_RM_15JUL2013.dat", header=T, sep="\t")
prs21a <- merge(prs21, lbc_bld[,c(1,3)], by.x="IID", by.y="bloodnum")

tmp = merge(d21a, prs21a[,c("studyno","X1.000000")], by.x="ID_raw", by.y="studyno", all.x=T)

tmp1 <- tmp[,c(4,6,14,19:35)]

### remove anyone with missing cognitive data ###
a <- rowSums(tmp1[,5:8])
w1_21 <- tmp1[which(!is.na(a)), ]

### adjust lung function for age, sex, and height ###
w1_21$lung <- resid(lm(fev ~ age + sex + height, na.action=na.exclude, data=w1_21))

### PCA of cognitive test data at wave 1 of LBC36 ###
library(psych)
w1_21$g <- scale(principal(w1_21[,5:8], rotate="none", nfactors=1)$scores)

### PCA output for variance explained by g ###
principal(w1_21[,5:8], rotate="none", nfactors=4)

#######################################
### Main LBC1921 Prediction Results ###
#######################################

### correlation and variance explained ###
r <- cor(w1_21$g, w1_21$g_pred, use="pairwise.complete.obs")
r

### incremental DNAm R2 ###
null <- summary(lm(g ~ age + sex, data=w1_21))$r.squared
full <- summary(lm(g ~ age + sex + g_pred, data=w1_21))$r.squared
round(100*(full - null), 3)

### incremental PRS R2 ###
full2 <- summary(lm(g ~ age + sex + X1.000000, data=w1_21))$r.squared
round(100*(full2 - null), 3)

### incremental DNAm + PRS R2 ###
full3 <- summary(lm(g ~ age + sex + g_pred + X1.000000, data=w1_21))$r.squared
round(100*(full3 - null), 3)

### get pvals ###
summary(lm(g ~ age + sex + g_pred, data=w1_21))
summary(lm(g ~ age + sex + X1.000000, data=w1_21))

### difference in g between top and bottom quintile of DNAm_g ###
w1_21$g_pred_dec <- as.numeric(cut(w1_21$g_pred, quantile(w1_21$g_pred, prob = seq(0, 1, length = 6)), include.lowest=T ))
summary(lm(g ~ age + sex + factor(g_pred_dec), data=w1_21))



### Fully adjusted incremental DNAm R2 ###
nullf <- summary(lm(g ~ age + sex + log(bmi) + yrseduc + factor(smoker) + log(alcpw+1) + factor(hyphist) + HADSD + soclcode + log(sixmtime) + gripstr + lung, data=w1_21))$r.squared
fullf <- summary(lm(g ~ age + sex + log(bmi) + yrseduc + factor(smoker) + log(alcpw+1) + factor(hyphist) + HADSD + soclcode + log(sixmtime) + gripstr + lung + g_pred, data=w1_21))$r.squared
round(100*(fullf - nullf), 3)

### Fully adjusted incremental PRS R2 ###
full2f <- summary(lm(g ~ age + sex + log(bmi) + yrseduc + factor(smoker) + log(alcpw+1) + factor(hyphist) + HADSD + soclcode + log(sixmtime) + gripstr + lung + X1.000000, data=w1_21))$r.squared
round(100*(full2f - nullf), 3)

### Fully adjusted incremental DNAm + PRS R2 ###
full3f <- summary(lm(g ~ age + sex + log(bmi) + yrseduc + factor(smoker) + log(alcpw+1) + factor(hyphist) + HADSD + soclcode + log(sixmtime) + gripstr + lung + g_pred + X1.000000, data=w1_21))$r.squared
round(100*(full3f - nullf), 3)


### get pvals ###
summary(lm(g ~ age + sex + log(bmi) + yrseduc + factor(smoker) + log(alcpw+1) + factor(hyphist) + HADSD + soclcode + log(sixmtime) + gripstr + lung + g_pred, data=w1_21))
summary(lm(g ~ age + sex + log(bmi) + yrseduc + factor(smoker) + log(alcpw+1) + factor(hyphist) + HADSD + soclcode + log(sixmtime) + gripstr + lung + X1.000000, data=w1_21))






#############################
### Supplementary Table 8 ###
#############################

### Regression of covariates on DNAm g score ###
summary(lm(scale(g_pred) ~ scale(age) + sex + scale(yrsedu_w1) + scale(log(bmi_w1)) + factor(smokcat_w1) + scale(log(alcunitwk_w1+1)) + hibp_w1 + scale(hadsd_w1) + scale(depind_w1) + scale(log(sixmwk_w1)) + scale(grip) + scale(lung), data=w1_36))$coefficients[,c(1,2,4)]
nobs(lm(scale(g_pred) ~ scale(age) + sex + yrsedu_w1 + scale(bmi_w1) + factor(smokcat_w1) + scale(alcunitwk_w1) + hibp_w1 + scale(hadsd_w1) + scale(depind_w1) + scale(sixmwk_w1) + scale(grip) + scale(lung), data=w1_36))

summary(lm(scale(g_pred) ~ scale(age) + sex + scale(yrsedu_w1) + scale(log(bmi_w1)) + factor(smokcat_w1) + scale(log(alcunitwk_w1+1)) + hibp_w1 + scale(hadsd_w1) + scale(depind_w1) + scale(log(sixmwk_w1)) + scale(grip) + scale(lung), data=w1_36))$r.squared
summary(lm(scale(g_pred) ~ scale(age) + sex, data=w1_36))$r.squared

w1_21$smoker1 <- relevel(as.factor(ifelse(w1_21$smoker==1, "current", ifelse(w1_21$smoker==2, "never", ifelse(w1_21$smoker==3, "ex", "NA")))), ref="never")
summary(lm(scale(g_pred) ~ scale(age) + sex + scale(yrseduc) + scale(log(bmi)) + factor(smoker1) + scale(log(alcpw+1)) + factor(hyphist) + scale(HADSD) + scale(soclcode) + scale(log(sixmtime)) + scale(gripstr) + scale(lung), data=w1_21))$coefficients[,c(1,2,4)]
nobs(lm(g ~ age + sex + bmi + yrseduc + factor(smoker1) + alcpw + factor(hyphist) + HADSD + soclcode + sixmtime + gripstr + lung + g_pred + X1.000000, data=w1_21))

summary(lm(scale(g_pred) ~ scale(age) + sex + scale(yrseduc) + scale(log(bmi)) + factor(smoker1) + scale(log(alcpw+1)) + factor(hyphist) + scale(HADSD) + scale(soclcode) + scale(log(sixmtime)) + scale(gripstr) + scale(lung), data=w1_21))$r.squared
summary(lm(scale(g_pred) ~ scale(age) + sex, data=w1_21))$r.squared

